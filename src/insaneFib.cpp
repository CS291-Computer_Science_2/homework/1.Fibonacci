/*
	Works up to FIB[44] only
	Original author: kien_koi_1997 http://codeforces.com/blog/entry/14516
	Modified by: Jose Madureira
*/


#include <stdio.h>
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>
#include <cstdint>		//uint64
#include <cinttypes>	//printf uint64
using namespace std;

const unsigned long int M = 1000000007; // modulo

map<uint64_t, uint64_t> F;
vector<uint64_t> T[1234];

uint64_t f(int n, int Depth) {
	
	T[Depth].push_back(n);
	if (F.count(n)) return F[n];
	
	int k=n/2;
	
	if (n%2==0) { // n=2*k
		return F[n] = (f(k, Depth+1)*f(k, Depth+1) + f(k-1, Depth+1)*f(k-1, Depth+1)) % M;
	} else { // n=2*k+1
		return F[n] = (f(k, Depth+1)*f(k+1, Depth+1) + f(k-1, Depth+1)*f(k, Depth+1)) % M;
	}
}

int main(){
	int n = 0;
	uint64_t number = 0;
	
	F[0] = 1;
	F[1] = 1;
	
	cout<<"\nWhich Fibonacci number to find? ";
	cin >> n;
	
	if (n==0) cout<<0<<endl;	//just 0
	else  umber = f(n-1, 0);	//find fibonacci number
	
	printf("\n%"PRIu64"\n", number);	
	

	for (int i=0; i<1234; i++) {
		if (T[i].size() > 0) {
		
			sort(T[i].begin(), T[i].end());
			T[i].erase(unique(T[i].begin(), T[i].end()), T[i].end());
			
			printf("Depth[%d] : ", i);
			
			for (int j=0; j<T[i].size(); j++) {
				printf("%"PRIu64" ", T[i][j]);
			}
				
			printf("\n");
		}
	}
}
