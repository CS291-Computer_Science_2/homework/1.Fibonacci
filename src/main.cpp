#include <iostream>
using namespace std;

int Fib(int);

int main() {
	string again="y";
	char yn = again[0];
	int n=0;

	while (yn=='y' || yn=='Y') {
		cout <<  "\n\t\t###Fibanacci series calculator###"
		     << "\n\t\tEnter a number between 1 and 40: ";
		cin >> n;
		cin.clear();

		if (n>40)
			cout << "\tThe number you entered: " << n << " is greater than 40, try again.\n";
		else if (n<1)
			cout << "\tThe number you entered: " << n << " is less than 1, try again.\n";
		else {
			cout << "\t\t\tFibonacci of " << n << " is ";
			if (n>0)
				cout << "= " << Fib(n);
		}

		cin.get();
		cout << "\n\t\t\tGo again?  ";

		getline(cin, again);
		yn = again[0];
	}

	system("PAUSE");
	return 0;
}

int Fib(int i) {
	if (i<=1) return 1;
	return Fib(i-1) + Fib(i-2);
}
